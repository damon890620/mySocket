package com.mySocket;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class Processor extends Thread {

	
	private static final String WEB_ROOT = "E:/JeecgWorkSpace/mySocket/view";
	private Socket socket;
	PrintStream printStream = null;
//	public static void main(String[] args) {
//		new Processor();
//	}
//	
	
	public Processor(Socket socket) {
		this.socket = socket;
		try {
			printStream = new PrintStream(socket.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@Override
	public void run(){
		if(socket!=null){
			System.out.println("服务器已开启");
//				根据socket获取请求文件名称
			String filename = 	requestUrl(socket);
			String path = WEB_ROOT + filename;
//				响应请求文件
			sendFile(socket,path);
		}
	}
	
	public String requestUrl(Socket socket){
		String filename = null;
		try {
//			根据socket 获取输入流
			InputStreamReader inputStreamReader =  new InputStreamReader(socket.getInputStream());
//			根据获取的输入流 获取BufferReader
			BufferedReader reader = new BufferedReader(inputStreamReader);
//			根据获取的 BufferReader 获取行信息
			String line = reader.readLine();
//			根据空格符将行信息分成数组
			String [] content = line.split(" ");
			System.out.println(content[0]);
			System.out.println(content[1]);
			System.out.println(content[2]);
			filename = content[1];
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filename;
	}
	
	/**
	 * 
	* @Title: sendFile 
	* @Description: 根据文件路径、socket 获取文件并输出
	* @param @param socket
	* @param @param path    设定文件 
	* @return void    返回类型 
	* @author Damon·Jing
	* QQ:2811001300
	* @throws
	 */
	public void sendFile(Socket socket,String path){
//		根据文件路径获取请求文件内容
		File file = new File(path);
		try {
			 if (file.exists() && !file.isDirectory()) {
//				 	输出头部信息
					printStream.println("HTTP/1.0 200 OK");
					printStream.println("Content-Type:text/html; charset-GBK");
					printStream.println("Content-Length:" + file.length());
					printStream.println();//http 协议规定报文头已空行结尾
					
//					根据文件获取文件流
					FileInputStream fileInputStream = new FileInputStream(file);
					byte[] buf = new byte[fileInputStream.available()];
					fileInputStream.read(buf);
					printStream.write(buf);
					
//					关闭输入流输出流
					printStream.close();
					fileInputStream.close();
			 }else{
				 sendError(404, "服务器找不到请求路径");
				 return;
			 }
		 } catch (IOException e) {
			e.printStackTrace();
		 }
	}
	
	/**
	 * 
	* @Title: sendError 
	* @Description: 请求找不到路径时返回错误信息
	* @param @param errorCode
	* @param @param msg    设定文件 
	* @return void    返回类型 
	* @author Damon·Jing
	* QQ:2811001300
	* @throws
	 */
	public void sendError(Integer errorCode,String msg){
//		输出头部信息
		printStream.println("HTTP/1.0 "+errorCode+" errorMesg:"+msg);
		printStream.println("Content-Type:text/html; charset-GBK");
		printStream.println();//http协议中 头部信息以空行结尾
//		输出html文本信息
		printStream.println("<html> "
				+ "<head> "
				+ "<title>Error Page </title> "
				+ "</head>"
				+ " <body> <center>"
				+ "<h1> Error-Code:" + errorCode +  "</h1> <h1>   Error-Msg:" +msg+"</h1>"
				+ "</body> "
				+ "</html>");
		printStream.close();
	}
	
 
	
}
