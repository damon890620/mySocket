package com.mySocket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServer {

	private static final Integer PORT = 8877;
	private ServerSocket serversocket = null;
	
	public WebServer(){
		 try {
//				根据端口创建一个ServerSocket监听 PORT端口
			serversocket = new ServerSocket(PORT);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void serverStart(){
		Socket socket = null;
//		持续监听 端口
		while(true){
			try {
	//				创建一个Socket连接
					socket = serversocket.accept();
					System.out.println("新增连接："+socket.getInetAddress()+":"+socket.getPort());
	//				调用处理端口请求的服务
					new Processor(socket).start();
				
					continue;
				}
			catch (IOException e) {
				e.printStackTrace();
			}
		} 
	}
	
	
	public static void main(String[] args) {
		new WebServer().serverStart();
}
}
